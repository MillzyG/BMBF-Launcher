#ifndef BMBFLAUNCHERWINDOW_H
#define BMBFLAUNCHERWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class BMBFLauncherWindow; }
QT_END_NAMESPACE

class BMBFLauncherWindow : public QMainWindow
{
    Q_OBJECT

public:
    BMBFLauncherWindow(QWidget *parent = nullptr);
    ~BMBFLauncherWindow();

private slots:
    void on_Save_IP_clicked();

    void on_Start_Button_clicked();

private:
    Ui::BMBFLauncherWindow *ui;
};
#endif // BMBFLAUNCHERWINDOW_H
